import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
//        1. Jabłka - potrzebują ciemności i niewysokich temperatur i nie mogą być w okolicy detergentów.
//        2. Świece - potrzebują ciemności i nie mogą być narażone na wstrząsy, nie mogą być w okolicy detergentów.
//        3. Kaczy puch - jest neutralny.
//        4. Ubrania - są neutralne.
//        5. Detergenty - Potrzebują chłodu i braku wstrząsów, nie mogą być w obecności bliskiej świec.
//        6. Barwniki spożywcze - nie mogą być w okolicy detergentów.
//        7. Pokarm dla ryb - neutralny.
//        8. Smary - nie mogą być w okolicy barwników ani w niskich temperaturach.
//        9. Cebulki kwiatowe - potrzebują światła ale też niższych temperatur.
//        10. Donice - muszą być blisko powierzchni ze względu na chęć producenta do powstałych uszczerbków z powodu słonej wody.
//        11. Kulki magnetyczne - powinny być z dala od wstrząsów oraz na powierzchni.
//        12. Prezerwatywy - muszą być w cieple i na świetle, Rikimaki jest przeciwny antykoncepcji.
public class Main {
    public static void main(String[] args) {
        Ship newShip = new Ship();
        newShip.chooseSection(new Material(MATERIAL_TYPE.NEUTRAL,MATERIAL_TYPE.CONDOMS,PARAMETER.WARM,PARAMETER.SUNLIGHT));
        newShip.chooseSection(new Material(MATERIAL_TYPE.NEUTRAL,MATERIAL_TYPE.MAGNETICBALL,PARAMETER.NOVIBRATIONS,PARAMETER.SUNLIGHT));
        newShip.chooseSection(new Material(MATERIAL_TYPE.CANDLES,MATERIAL_TYPE.DETERGENTS,PARAMETER.COLD,PARAMETER.NOVIBRATIONS));
        newShip.chooseSection(new Material(MATERIAL_TYPE.DETERGENTS,MATERIAL_TYPE.CANDLES,PARAMETER.NOSUNLIGHT,PARAMETER.NOVIBRATIONS));
        newShip.printer();
    }
    }




