import java.util.ArrayList;

public class Material {
    private MATERIAL_TYPE type;
    private ArrayList<PARAMETER> parameterList;
    private MATERIAL_TYPE notWelcomeType;

    public Material(MATERIAL_TYPE notWelcome, MATERIAL_TYPE type, PARAMETER... parameter) {
        ArrayList<PARAMETER> tmp = new ArrayList<>();


        for (int i = 0; i <parameter.length; i++) {
            tmp.add(parameter[i]);
        }
        parameterList=tmp;
        this.notWelcomeType = notWelcome;
        this.type = type;
    }

    public ArrayList<PARAMETER> getParameterList() {
        return parameterList;
    }

    public MATERIAL_TYPE getType() {
        return type;
    }

    public MATERIAL_TYPE getNotWelcomeType() {
        return notWelcomeType;
    }
}
