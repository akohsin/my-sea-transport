import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Ship {
    private HashMap<Integer, Section> sectionsList = new HashMap<>();

    public Ship() {
        this.sectionsList.put(1, new Section(PARAMETER.SUNLIGHT, PARAMETER.VIBRATIONS, PARAMETER.COLD, PARAMETER.SALTY));
        this.sectionsList.put(2, new Section(PARAMETER.SUNLIGHT, PARAMETER.NOVIBRATIONS, PARAMETER.COLD, PARAMETER.SALTY));
        this.sectionsList.put(3, new Section(PARAMETER.SUNLIGHT, PARAMETER.NOVIBRATIONS, PARAMETER.WARM, PARAMETER.SALTY));
        this.sectionsList.put(4, new Section(PARAMETER.NOSUNLIGHT, PARAMETER.VIBRATIONS, PARAMETER.COLD, PARAMETER.NOTSALTY));
        this.sectionsList.put(5, new Section(PARAMETER.NOSUNLIGHT, PARAMETER.NOVIBRATIONS, PARAMETER.COLD, PARAMETER.NOTSALTY));
        this.sectionsList.put(6, new Section(PARAMETER.NOSUNLIGHT, PARAMETER.NOVIBRATIONS, PARAMETER.WARM, PARAMETER.NOTSALTY));
    }

    public void chooseSection(Material material) {
        ArrayList<PARAMETER> materialParameters = material.getParameterList();
        MATERIAL_TYPE notWelcome = material.getNotWelcomeType();
        for (Map.Entry<Integer, Section> entry : sectionsList.entrySet()) {
            boolean goodChoice = false;
            int left = onTheLeft(entry.getKey());
            int right = onTheRight(entry.getKey());

            if (entry.getValue().isEmpty()) {
                if (entry.getValue().getParameterList().containsAll(materialParameters)) {
                    goodChoice = true;
                    if (left > 0 && !sectionsList.get(left).isEmpty()) {
                        MATERIAL_TYPE tmp = sectionsList.get(left).getType();
                        if (tmp.equals(notWelcome)) {
                            goodChoice = false;
                        }
                    }
                    if (right > 0 && !sectionsList.get(right).isEmpty()) {
                        MATERIAL_TYPE tmp2 = sectionsList.get(right).getType();
                        if (tmp2.equals(notWelcome)) {
                            goodChoice = false;
                        }
                    }
                    if (goodChoice) {
                        entry.getValue().fillSection(material.getType());
                        break;
                    }
                }
            }
        }
    }

    public void printer() {
        for (Map.Entry<Integer, Section> entry : sectionsList.entrySet()) {
            System.out.print(entry.getValue().getType() + " ");
            if (onTheRight(entry.getKey()) == 0) {
                System.out.println();
            }

        }
    }


    public int onTheLeft(int numer) {
        switch (numer) {
            case 1:
                return 0;
            case 2:
                return 1;
            case 3:
                return 2;
            case 4:
                return 0;
            case 5:
                return 4;
            case 6:
                return 5;
        }
        return 0;
    }

    public int onTheRight(int numer) {
        switch (numer) {
            case 1:
                return 2;
            case 2:
                return 3;
            case 3:
                return 0;
            case 4:
                return 5;
            case 5:
                return 6;
            case 6:
                return 0;
        }
        return 0;
    }

}
