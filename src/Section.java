import java.util.ArrayList;

public class Section {
    private ArrayList<PARAMETER> parameterList;
    private boolean empty;
    private MATERIAL_TYPE type;

    public Section(PARAMETER... parameter) {
        ArrayList<PARAMETER> tmp = new ArrayList<>();


        for (int i = 0; i <parameter.length; i++) {
         tmp.add(parameter[i]);
        }
        parameterList=tmp;
        empty = true;
    }

    public ArrayList<PARAMETER> getParameterList() {
        return parameterList;
    }


    public boolean isEmpty() {
        return empty;
    }

    public MATERIAL_TYPE getType() {
        return type;
    }

    public void fillSection(MATERIAL_TYPE type) {
        this.type = type;
        this.empty = false;
    }
}
